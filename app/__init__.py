from flask import Flask, Blueprint
from flask.ext.mongoengine import MongoEngine
from flask.ext.bootstrap import Bootstrap
from flask.ext.login import LoginManager

app = Flask(__name__)

db = MongoEngine(app)
bootstrap = Bootstrap(app)
lm = LoginManager(app)
# config
app.config["MONGODB_SETTINGS"] = {'DB': "reza-v01"}
app.config["SECRET_KEY"] = "secret"


# import blueprints
from creator import creator as creator_blueprint
app.register_blueprint(creator_blueprint)


if __name__ == '__main__':
    app.run(debug=True)