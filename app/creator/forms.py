from flask.ext.wtf import Form
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import required, length, EqualTo


class CreatorLogin(Form):
    username = StringField('Username', validators=[required(), length(1, 15)])
    password = PasswordField('Password', validators=[required(), length(1, 30)])
    remember_me = BooleanField('Remember me')
    submit = SubmitField('Submit')


class CreatorSignup(Form):
	username = StringField('Username', validators=[required(), length(1,15)])
	password = PasswordField('Password', validators=[required(), length(1,30)])
	confirm_password = PasswordField('Confirm Password', validators=[EqualTo('password', message='Passwords must match')])
	submit = SubmitField('Submit')
